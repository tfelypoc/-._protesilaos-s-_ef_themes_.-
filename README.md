# Ef (εὖ) themes for GNU Emacs

WORK-IN-PROGRESS: screenshots will be available once things stabilise.

The `ef-themes` are a collection of light and dark themes for GNU Emacs
whose goal is to provide colorful ("pretty") yet legible options for
users who want something with a bit more flair than the `modus-themes`
(also designed by me).

+ Package name (GNU ELPA): `ef-themes`
+ Official manual: <https://protesilaos.com/emacs/ef-themes>
+ Git repo on SourceHut: <https://git.sr.ht/~protesilaos/ef-themes>
  - Mirrors:
    + GitHub: <https://github.com/protesilaos/ef-themes>
    + GitLab: <https://gitlab.com/protesilaos/ef-themes>
+ Mailing list: <https://lists.sr.ht/~protesilaos/ef-themes>
